/**
 * represents worker (miner)
 * 
 * implements Runnable - run method performs process of mining
 */
public class Worker implements Runnable {
    public static int MAX_MINING_SOURCE_TIME;
    private SourceBlock assignedBlock;
    private int sourcesMinedCount = 0;
    private Thread thread;

    private static int ID_COUNTER = 1;
    private int id;

    public Worker() {
        id = ID_COUNTER++;
    }

    /**
     * Asks Forman from blocks, mines and load them on ferry until null is returned
     */
    @Override
    public void run() {
        while ((assignedBlock = Foreman.getForeman().assignBlock()) != null) {
            mine();
            loadBlockOnLorry();
        }
    }

    /**
     * mines assigned block fully
     */
    private void mine() {
        long startBlock = System.currentTimeMillis();
        while (!assignedBlock.isMined()) {
            long startSource = System.currentTimeMillis();

            try {
                Thread.sleep(getRandomTimeMining());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            assignedBlock.mineOneSource();

            long timeElapsedSource = System.currentTimeMillis() - startSource;
            Main.LOG.log("Worker", id, "Source mined", timeElapsedSource);
            sourcesMinedCount++;
        }
        long timeElapsedBlock = System.currentTimeMillis() - startBlock;
        Main.LOG.log("Worker", id, "Block mined", timeElapsedBlock);
    }

    /**
     * returns random time of mining
     * 
     * @return
     */
    private long getRandomTimeMining() {
        return (long) ((Math.random() * MAX_MINING_SOURCE_TIME) + 1);
    }

    /**
     * loads all blocks on lorry in mine
     */
    private void loadBlockOnLorry() {
        for (int source = 0; source < assignedBlock.getSize(); source++) {
            Lorry.loadSourceToLorryInMine();
        }
    }

    public int getMinedSourcesCount() {
        return sourcesMinedCount;
    }

    public Thread getThread() {
        return thread;
    }

    public void setThread(Thread thread) {
        this.thread = thread;
    }

    public SourceBlock getAssignedBlock() {
        return assignedBlock;
    }

    @Override
    public String toString() {
        return "Worker " + id;
    }

}
