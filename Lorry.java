import java.util.ArrayList;

/**
 * Represents Lorry, one lorry is always in mine
 * 
 * implements Runnable - run method performs transport to destination
 * 
 */
public class Lorry implements Runnable {
    private static final int LOAD_TIME = 1000;

    public static int MAX_TRANSPORT_TIME;
    public static int CAPACITY;

    private static int ID_COUNTER = 1;
    private int id;

    /**
     * all lorries
     */
    private static ArrayList<Lorry> allLorries = new ArrayList<Lorry>();

    private int loadedSourcesCount = 0;
    private LorryState state = LorryState.IN_MINE;
    private Thread thread;
    private long inMainTimestamp = System.currentTimeMillis();

    private Lorry() {
        id = ID_COUNTER++;
    }

    /**
     * loads one source to lorry in mine
     * 
     * synchronized
     */
    public synchronized static void loadSourceToLorryInMine() {
        getLorryByState(LorryState.IN_MINE).loadSource();
    }

    /**
     * if more exists only first will be returned
     * 
     * if none exists null will be returned
     * 
     * @return lorry with @param state
     */
    public static Lorry getLorryByState(LorryState state) {
        ArrayList<Lorry> allByState = getAllLorriesByState(state);
        if (allByState.size() < 1) {
            return null;
        } else {
            return allByState.get(0);
        }
    }

    /**
     * @param state
     * @return all lorries with @param state
     */
    public static ArrayList<Lorry> getAllLorriesByState(LorryState state) {
        ArrayList<Lorry> filteredList = new ArrayList<>(allLorries);
        filteredList.removeIf(lorry -> lorry.getState() != state);
        return filteredList;
    }

    /**
     * @return all lorries
     */
    public static ArrayList<Lorry> getAllLorries() {
        return allLorries;
    }

    /**
     * prepares new lorry only if none in mine
     */
    public static void prepareNewLorry() {
        if (getLorryByState(LorryState.IN_MINE) == null) {
            allLorries.add(new Lorry());
        }
    }

    /**
     * load one source to lorry
     * 
     * set to transport a prepare new lorry, if lorry is full
     */
    private void loadSource() {
        try {
            Thread.sleep(LOAD_TIME);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (loadedSourcesCount < CAPACITY) {
            loadedSourcesCount++;
            if (loadedSourcesCount == CAPACITY) {
                transport();
                prepareNewLorry();
            }
        } else {
            System.out.println("Error unexpected state - loading to full Lorry");
        }
    }

    /**
     * @return state of lorry
     */
    public LorryState getState() {
        return state;
    }

    /**
     * @return lorry thread
     */
    public Thread getThread() {
        return thread;
    }

    /**
     * @return loaded sources on lorry
     */
    public int getLoadedSourcesCount() {
        return loadedSourcesCount;
    }

    /**
     * starts the thread method run
     */
    public void transport() {
        this.state = LorryState.TRANSPORTING;
        this.thread = new Thread(this);
        long timeElapsedInMain = (System.currentTimeMillis() - inMainTimestamp);
        Main.LOG.log("Lorry", id, "Start transport to ferry", timeElapsedInMain);
        thread.start();
    }

    /**
     * transports to ferry and then to final destination
     */
    @Override
    public void run() {
        transportToFerry();
        transportByFerry();
        transportToFinalDestination();
    }

    /**
     * transport to ferry
     */
    private void transportToFerry() {
        long transportTime = transportTimer(MAX_TRANSPORT_TIME - 1);
        Main.LOG.log("Lorry", id, "Arrived to the ferry", transportTime);
    }

    /**
     * loads to ferry and waits to be transported
     */
    private void transportByFerry() {
        Ferry.getFerry().transportLorry();
    }

    /**
     * transports to final destination
     */
    private void transportToFinalDestination() {
        long transportTime = transportTimer(MAX_TRANSPORT_TIME);
        this.state = LorryState.IN_FINAL_DESTINATION;
        Main.LOG.log("Lorry", id, "Arrived to the final destination", transportTime);
    }

    /**
     * simulates transport time - waits randomly between 1 - @param maxTime ms
     * 
     * @return time it took to transport
     */
    private long transportTimer(int maxTime) {
        long timeTransporting = (long) ((Math.random() * maxTime) + 1);
        long startTransport = System.currentTimeMillis();
        try {
            Thread.sleep(timeTransporting);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return System.currentTimeMillis() - startTransport;
    }

}
