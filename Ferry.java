/**
 * Represents Ferry
 * 
 * Singleton - only one instance exists
 */
public class Ferry {
    public static int CAPACITY;
    private static Ferry FERRY_SINGLETON = new Ferry();
    private long waitingTimestamp = System.currentTimeMillis();
    private int loadedLorries = 0;
    private boolean full = false;

    private Ferry() {
    }

    public static Ferry getFerry() {
        return FERRY_SINGLETON;
    }

    /**
     * loads one lorry - blocks until the ferry capacity is full
     * 
     * synchronized
     */
    public synchronized void transportLorry() {
        while (full) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        loadedLorries++;

        if (loadedLorries == CAPACITY) {
            full = true;
            setOff();
            notifyAll();
        }

        while (!full) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        loadedLorries--;

        if (loadedLorries == 0) {
            full = false;
            notifyAll();
        }

    }

    /**
     * Transports all loaded lorries
     */
    private void setOff() {
        long timeElapsedInMain = (System.currentTimeMillis() - waitingTimestamp);
        Main.LOG.log("Ferry", 1, "Set off", timeElapsedInMain);
        System.out.println("Ferry set off");
        waitingTimestamp = System.currentTimeMillis();
    }

}
