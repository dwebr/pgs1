import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Map of mine
 */
public class Map {
    /**
     * Array of sourceBlocks
     */
    private final SourceBlock[] blocks;
    /**
     * count of all sources in mine
     */
    private final int sourceCount;

    /**
     * Create map from file on disk
     * 
     * @param mapFileName path to the file
     * @throws Exception if file not found or is invalid
     */
    public Map(String mapFileName) throws Exception {
        String[] mapLines = getFileLines(mapFileName);
        ArrayList<SourceBlock> blocksAL = new ArrayList<SourceBlock>();
        int foundSources = 0;

        for (String line : mapLines) {
            int blockLength = 0;
            for (int i = 0; i < line.length(); i++) {
                char position = line.charAt(i);
                if (position == 'X') {
                    foundSources++;
                    blockLength++;
                } else if (position == ' ') {
                    if (blockLength > 0) {
                        blocksAL.add(new SourceBlock(blockLength));
                        blockLength = 0;
                    }
                } else {
                    throw new Exception("Invalid character");
                }
            }
            if (blockLength > 0) {
                blocksAL.add(new SourceBlock(blockLength));
            }
        }

        sourceCount = foundSources;
        SourceBlock[] blocksArr = new SourceBlock[blocksAL.size()];
        blocks = blocksAL.toArray(blocksArr);
    }

    /**
     * Open files and read each line
     * 
     * @param mapFileName path to file
     * @return array of lines
     * @throws IOException if file not found or error with access
     */
    private String[] getFileLines(String mapFileName) throws IOException {
        ArrayList<String> lines = new ArrayList<String>();
        BufferedReader br = new BufferedReader(new FileReader(mapFileName));
        try {
            String line = br.readLine();
            while (line != null) {
                lines.add(line);
                line = br.readLine();
            }
        } finally {
            br.close();
        }
        String[] linesArray = new String[lines.size()];
        return lines.toArray(linesArray);
    }

    /**
     * @return all blocks in map
     */
    public SourceBlock[] getBlocks() {
        return blocks;
    }

    /**
     * Finds unassigned block in map
     * 
     * @return unassigned block in map or null if all block assigned
     */
    public SourceBlock getFreeBlock() {
        return Arrays.stream(blocks).filter(block -> !block.isAssigned()).findFirst().orElse(null);
    }

    /**
     * @return sources count
     */
    public int getSourceCount() {
        return sourceCount;
    }

    @Override
    public String toString() {
        return "Number of sources and blocks: " + sourceCount + ", " + blocks.length;
    }

}
