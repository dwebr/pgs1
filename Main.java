import java.util.ArrayList;

public class Main {
    public static Logger LOG;
    public static Worker[] workers;

    /**
     * main method
     * 
     * @param args
     */
    public static void main(String[] args) {
        // read parameters
        Parameters par;
        try {
            par = new Parameters(args);
        } catch (Exception e) {
            System.out.println("Invalid arguments");
            System.out.println(e.getMessage());
            return;
        }
        System.out.println(par.toString());

        // create file logger
        LOG = new Logger(par.getOutputFileName());

        // read map
        Map map;
        try {
            map = new Map(par.getInputFileName());
        } catch (Exception e) {
            System.out.println("Map is invalid");
            System.out.println(e.getMessage());
            LOG.close();
            return;
        }
        System.out.println(map.toString());
        System.out.println();
        LOG.log("Foreman", 1, map.toString());

        // set workers
        Worker.MAX_MINING_SOURCE_TIME = par.getWorkerTime();
        createWorkers(par.getWorkerCount());

        // set lorry
        Lorry.CAPACITY = par.getLorryCapacity();
        Lorry.MAX_TRANSPORT_TIME = par.getLorryTime();
        Lorry.prepareNewLorry();

        // set ferry
        Ferry.CAPACITY = par.getFerryCapacity();

        // ser foreman
        Foreman foreman = Foreman.createForeman(map, workers);
        foreman.sendWorkersToMine();

        // wait for workers and lorries threads to end
        waitOnWorkers();
        waitOnLorries();

        printSummary();

        LOG.close();
    }

    /**
     * create workers instances
     * 
     * @param workersCount number of workers
     */
    private static void createWorkers(int workersCount) {
        workers = new Worker[workersCount];
        for (int index = 0; index < workers.length; index++) {
            workers[index] = new Worker();
        }
    }

    /**
     * wait for all workers threads to end
     */
    private static void waitOnWorkers() {
        for (Worker worker : workers) {
            Thread workerThread = worker.getThread();
            try {
                workerThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * wait on all lorries threads to end
     */
    private static void waitOnLorries() {
        for (Lorry lorry : Lorry.getAllLorries()) {
            Thread lorryThread = lorry.getThread();
            if (lorryThread != null) {
                try {
                    lorryThread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * prints summary
     */
    private static void printSummary() {
        System.out.println();
        for (Worker worker : workers) {
            String message = worker.toString() + " sources mined: " + worker.getMinedSourcesCount();
            System.out.println(message);
        }
        ArrayList<Lorry> lorriesInDestination = Lorry.getAllLorriesByState(LorryState.IN_FINAL_DESTINATION);
        int sourcesInDestination = 0;
        for (Lorry lorry : lorriesInDestination) {
            sourcesInDestination += lorry.getLoadedSourcesCount();
        }
        System.out.println("Sources in final destination: " + sourcesInDestination);
    }
}
