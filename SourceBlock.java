/**
 * Represents one block of sources in mine
 */
public class SourceBlock {
    private boolean assigned = false;
    private boolean mined = false;
    private final int size;
    private int sourcesMined;

    public SourceBlock(int size) {
        this.size = size;
    }

    /**
     * mines one source
     */
    public void mineOneSource() {
        if (sourcesMined < size) {
            sourcesMined++;
            if (sourcesMined == size) {
                mined = true;
            }
        } else {
            System.out.println("Error unexpected state - mining mined source");
        }
    }

    public boolean isAssigned() {
        return assigned;
    }

    public void setAssigned() {
        this.assigned = true;
    }

    public boolean isMined() {
        return mined;
    }

    public int getSize() {
        return size;
    }

}
