import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * logs messages into file
 */
public class Logger {
    private final long timestampZero;
    private PrintWriter logWriter;
    DateFormat formatterTimestamp = new SimpleDateFormat("mm:ss.SSS");

    /**
     * Creates logger
     * 
     * @param logFileName filename to write logs
     */
    public Logger(String logFileName) {
        try {
            logWriter = new PrintWriter(logFileName, "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        formatterTimestamp.setTimeZone(TimeZone.getTimeZone("UTC"));
        timestampZero = System.currentTimeMillis();

    }

    private String getTimeStamp() {
        long timeElapsedMill = System.currentTimeMillis() - timestampZero;
        Date timeElapsed = new Date(timeElapsedMill);
        return formatterTimestamp.format(timeElapsed);
    }

    /**
     * writes message to file
     * 
     * @param role
     * @param message
     */
    public void log(String role, int id, String message, long durationMillis) {
        Date timeDuration = new Date((long) durationMillis);
        String logMessage = message + ";" + formatterTimestamp.format(timeDuration);
        log(role, id, logMessage);
    }

    public void log(String role, int id, String message) {
        String logMessage = getTimeStamp() + ";" + role + ";" + id + ";" + message;
        logWriter.println(logMessage);
        logWriter.flush();
    }

    public void close() {
        logWriter.close();
    }
}
