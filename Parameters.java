/**
 * parses and represents program input parameters
 */
public class Parameters {
    String[] args;
    private final String inputFileName;
    private final String outputFileName;
    private final int workerCount;
    private final int workerTime;
    private final int lorryCapacity;
    private final int lorryTime;
    private final int ferryCapacity;

    /**
     * Creates instance representing input parameters
     * 
     * @param args input arguments of program
     * @throws Exception if arguments are invalid or incomplete
     */
    public Parameters(String[] args) throws Exception {
        this.args = args;
        inputFileName = getArgument("-i");
        outputFileName = getArgument("-o");
        workerCount = Integer.parseInt(getArgument("-cWorker"));
        workerTime = Integer.parseInt(getArgument("-tWorker"));
        lorryCapacity = Integer.parseInt(getArgument("-capLorry"));
        lorryTime = Integer.parseInt(getArgument("-tLorry"));
        ferryCapacity = Integer.parseInt(getArgument("-capFerry"));
        if (workerCount <= 0) {
            throw new Exception("Invalid worker count");
        }
        if (workerTime < 0) {
            throw new Exception("Invalid worker time");
        }
        if (lorryCapacity <= 0) {
            throw new Exception("Invalid lorry capacity");
        }
        if (lorryTime < 0) {
            throw new Exception("Invalid lorry time");
        }
        if (ferryCapacity <= 0) {
            throw new Exception("Invalid ferry capacity");
        }
    }

    /**
     * returns actual arguments by argument name
     * 
     * @param argumentType type of argument
     * @return actual argument
     * @throws Exception if argument not found
     */
    private String getArgument(String argumentType) throws Exception {
        for (int index = 0; index < args.length - 1; index += 2) {
            String argType = args[index];
            if (argType.equals(argumentType)) {
                return args[index + 1];
            }
        }
        throw new Exception("Argument " + argumentType + " missing");
    }

    public String getInputFileName() {
        return inputFileName;
    }

    public String getOutputFileName() {
        return outputFileName;
    }

    public int getWorkerCount() {
        return workerCount;
    }

    public int getWorkerTime() {
        return workerTime;
    }

    public int getLorryCapacity() {
        return lorryCapacity;
    }

    public int getLorryTime() {
        return lorryTime;
    }

    public int getFerryCapacity() {
        return ferryCapacity;
    }

    @Override
    public String toString() {
        return "Input file name: " + inputFileName + "\nOutput file name: " + outputFileName + "\nWorker count: "
                + workerCount + "\nWorker time: " + workerTime + "\nLorry capacity: " + lorryCapacity + "\nLorry time: "
                + lorryTime + "\nFerry capacity: " + ferryCapacity;
    }

}
