/**
 * represents foreman(farmer) that manages miners(workers)
 * 
 * singleton - only one instance exists
 */
public class Foreman {
    private static Foreman FOREMAN_SINGLETON;
    private final Map map;
    private final Worker[] workers;

    /**
     * creates foreman
     * 
     * @param map     map of mine
     * @param workers miners
     * @return instance of foreman
     */
    public static Foreman createForeman(Map map, Worker[] workers) {
        FOREMAN_SINGLETON = new Foreman(map, workers);
        return FOREMAN_SINGLETON;
    }

    private Foreman(Map map, Worker[] workers) {
        this.map = map;
        this.workers = workers;
    }

    public static Foreman getForeman() {
        return FOREMAN_SINGLETON;
    }

    /**
     * starts workers threads
     */
    public void sendWorkersToMine() {
        for (Worker worker : workers) {
            Thread workerThread = new Thread(worker);
            worker.setThread(workerThread);
            workerThread.start();
        }
    }

    /**
     * @return how many workers have still a block assigned
     */
    private int getWorkingWorkersCount() {
        int count = 0;
        for (Worker worker : workers) {
            if (worker.getAssignedBlock() != null) {
                count++;
            }
        }
        return count;
    }

    /**
     * synchronized
     * 
     * @return unassigned block and mark it assigned
     */
    public synchronized SourceBlock assignBlock() {
        SourceBlock sourceBlock = map.getFreeBlock();
        if (sourceBlock != null) {
            sourceBlock.setAssigned();
        } else if (getWorkingWorkersCount() == 1) {
            forceTransportLorryInMine();
        }
        return sourceBlock;
    }

    /**
     * force lorry in mine to transport if not empty
     */
    private void forceTransportLorryInMine() {
        Lorry lorryInMine = Lorry.getLorryByState(LorryState.IN_MINE);
        if (lorryInMine.getLoadedSourcesCount() != 0) {
            lorryInMine.transport();
        }
    }
}
